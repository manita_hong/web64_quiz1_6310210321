import logo from './logo.svg';
import './App.css';
import {Routes,Route} from "react-router-dom";
import Header from './components/Header';
import AboutUsPage from './page/AboutUsPage';
import OddEvenPage from './page/OddEvenPage';

function App() {
  return (
    <div className="App">
      <Header/>

      <Routes>

      <Route path="about" element={
                  <AboutUsPage/>
                  }/>
      <Route path="/" element={
                  <OddEvenPage/>
                  }/>
         
      </Routes>
    </div>
  );
}

export default App;
