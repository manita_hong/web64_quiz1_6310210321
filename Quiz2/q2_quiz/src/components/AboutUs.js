import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
function AboutUs (){

    return (
        <div>
             <Box sx={{width:"40%"}}>
             <Paper elevation={3}>
            <h3>จัดทำโดย : นางสาวมานิตา  แก้วปิยรัตน์ </h3>
            <h3>รหัสนักศึกษา : 6310210321 </h3>
            <h3>สถานศึกษา : มหาวิทยาลัยสงขลานครินทร์</h3>
            <h3>ติดต่อได้ที่ : 09377126XX</h3>
            <h3>ที่อยู่ : อ.หาดใหญ่ จ.สงขลา</h3>
            </Paper>
            </Box>
        </div>
    );
}

export default AboutUs;