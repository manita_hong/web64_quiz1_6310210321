import {Link} from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

function Header(){

return (
<Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
        
    <Typography variant="h6">ยินดีต้อนรับสู่เว็บวิเคราะห์เลขคู่ หรือ เลขคี่ : 
    </Typography>
    &nbsp;&nbsp;&nbsp;
    <Link to="/">
    <Typography variant="body1">เครื่องวิเคราะห์เลขคู่/คี่</Typography>
    </Link>
    &nbsp;&nbsp;&nbsp;
    <Link to="/about">
    <Typography variant="body1">ผู้จัดทำ</Typography>
    
    </Link>
    
    

        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;

