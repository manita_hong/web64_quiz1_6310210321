import { useEffect, useState } from "react";
import OddEvenResult from "../components/OddEvenResult";
import Button from '@mui/material/Button';

function OddEvenPage(){
    const[numoddeven,setNumoddeven] = useState("");
    const[numresult,setNumresult] = useState("");

    function Calloddeven(){
        let num = parseInt(numoddeven);
        
        if(num==0){
            setNumresult("เลขเป็น ศูนย์")
        }
        else if(num%2==0){
            setNumresult("เลข คู่ !!")
        }
       
        else{
            setNumresult("เลข คี่ !!")
        }
    }

   return( 

    <div align = "left">
            <div align = "center">
            <h2>ยินดีต้อนรับสู่การคำนวณ เลขคู่ หรือ เลขคี่</h2>
            
            <br/><br/><h3>กรุณาใส่ตัวเลข : </h3>
            <input type = "text"
                              value={numoddeven}
                              onChange={ (e) => { setNumoddeven(e.target.value) } } />
            <br /><br/>
            <Button variant="contained" onClick={( ) => { Calloddeven()}}>
                ทาย
                </Button>

            
            
            
            { numresult != 0 &&
         <div>
            <br/> นี่ผลการวิเคราะห์
           <OddEvenResult
            result = {numresult}/>
         </div>
         }

            </div>
            
           
    </div>
 
   );
} 
export default OddEvenPage;