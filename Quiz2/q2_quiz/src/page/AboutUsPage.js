import AboutUs from "../components/AboutUs";

function AboutUsPage(){

    return(
        <div>
            <div align="center">
                <h2>คณะผู้จัดทำ เว็บนี้</h2>
                <AboutUs/>
                
            
            </div>
        </div>
    );

}

export default AboutUsPage;